import math

# TODO: You can get multiple targets by appending them to the list:
#       self.targets = []
#       Check if the target is already in list first though.
#       MIGHT FURTHER SLOW DOWN THE GAME.
#
# FIXME: Slows down the game. Target is 300 ents, 60 FPS.
def get_target(getter, min_dist, entities):
    '''For two points P1 = (x1, y1) and P2 = (x2, y2) in the Cartesian
    plane, the distance between P1 and P2 is defined as:

    P1 P2 = sqrt( (x2 - x1)^2 + (y2 - y1)^2 )'''

    closest = [min_dist, None]

    for e in entities:
        dx = getter.pos[0] - e.pos[0]
        dy = getter.pos[1] - e.pos[1]

        # The math.hypot() method returns the Euclidean norm. The Euclidian
        # norm is the distance from the origin to the coordinates given.
        #
        # These are slower:
        # math.sqrt((e.pos[0] - self.pos[0])**2 + (e.pos[1] - self.pos[1])**2)
        # self.pos.distance_to(pygame.math.Vector2(e.pos[0], e.pos[1]))

        dist = math.hypot(dx, dy)

        if dist < closest[0] and e is not getter:
            closest[0] = dist
            closest[1] = e

    return closest[1]
