import pygame
import math

# TODO: Place all general functions that operate on the grid here, like mouse
# convert, ent convert, etc.

def get_grid_id(pos, grid_cell_size, max_size_px, min_size_px=0):
    width = (max_size_px-min_size_px)/grid_cell_size

    # Hash function
    grid_cell = int((math.floor(pos[0]/grid_cell_size)) + (math.floor(pos[1]/grid_cell_size)) * width)

    return grid_cell


def get_grid_xy(entity, grid_id, grid_cell_size):
    return (grid_id % grid_cell_size, grid_id // grid_cell_size)


def get_pos_to_grid(pos, grid_cell_size):
    pos_to_grid = pygame.math.Vector2(pos[0] // grid_cell_size,
                                      pos[1] // grid_cell_size)

    return pos_to_grid


# NOTE: Matrix is a game grid, ex.: (2048, 2048), position is pos_to_grid: (12, 12)
def get_adj_grid_cells(matrix, pos_to_grid):
    adj = []

    for dx in range(-1, 2):
        for dy in range(-1, 2):
            range_x = range(0, int(matrix[0]))  # X bounds
            range_y = range(0, int(matrix[1]))  # Y bounds

            (new_x, new_y) = (pos_to_grid[0]+dx, pos_to_grid[1]+dy)  # adjacent cell

            if (new_x in range_x) and (new_y in range_y) and (dx, dy) != (0, 0):
                adj.append(pygame.Vector2((int(new_x), int(new_y))))

    return adj


def get_adj_grid_ids(adj_grid_cells, grid_cell_size, max_size_px, min_size_px=0):
    adj_ids = []
    width = (max_size_px-min_size_px)/grid_cell_size

    for pos in adj_grid_cells:
        adj_id = int((math.floor(pos[0])) + (math.floor(pos[1])) * width)
        adj_ids.append(adj_id)

    return adj_ids

# NOTE: grid_pos is a Vector2(x, y) where x and y are grid coordinates (12, 14), not position (324, 648)
# def get_grid_pos_from_id(grid_pos, grid_id, map_size/2): # 32 - one row
    # Convert back
    # (x,y) <-> ID
    # Z[64] x Z[64] <-> Z[64^2]

    # id = f(x, y) = x * 64 + y
    # (x, y) = g(id) = (id // 64, id % 64)
