
def overlap_check(collider_list):
    ''' The method rect.colliderect(r) checks if a rectangle rect collides
    with another rectangle r. If we want to know if there are any two
    overlapping rectangles, then we have to compare each rectangle with each
    other one. The number of comparisons increases as power of 2.

    https://pygame.readthedocs.io/en/latest/rect/rect.html#overlapping-rectangles'''

    n = len(collider_list)
    rects = [c.visrect for c in collider_list]

    intersecting = []
    for i in range(n-1):
        r0 = rects[i]
        for j in range(i+1, n):
            r1 = rects[j]
            if r0.colliderect(r1):
                intersecting.append(r0)
                intersecting.append(r1)
                break

    # print(rects, intersecting)

    return rects, intersecting

    # for i, r in enumerate(rects):
    #     # self.color = self.color.colors["gray"] if r in intersecting else self.color.colors["red"]
    #     if r in intersecting:
    #         print("Overlap")
    #         # return True
