# Changelog

## [0.2.0] - 2022-12-26

### Added
- 3x3 vision
- Performance issues

### Changed
- A lot of ugly stuff under the hood

### Fixed

## [0.1.2] - 2022-06-28

### Added
- Camera mouse controlls
- Prey and Predator classes
- Prey move_away state

### Changed
- Renamed Render class to Renderer
- Moved all main Renderer calls to update function
- Updated Debug UI
- Changed font from Oxanium to RobotoMono
- Made color scheme more bright and readable

### Fixed

## [0.1.1] - 2022-03-22

### Added
- Cells leave eatable corpses after death
- Add arrow keys to move camera

## [0.1] - 2022-03-22

- The functional part of old Cells restored
