# C2

![Screenshot](https://gitlab.com/mkmkbrs/c2/-/raw/main/screenshot.png)

**Cells 2** is a basic life-simulation environment made with Python 3 and Pygame 2.

## Controlls

| Keys                          | Action                    |
|:------------------------------|:--------------------------|
| WASD, HJKL, Arrow Keys, Mouse | Move Camera               |
| `                             | Enable/Disable Debug Info |
| q                             | Quit                      |

## Known issues

- Due to a bug, there is a chance a dead cell will become reanimated, resulting in a zombie party that can quickly overcome all other cells on the map. Zombie cells also use the reproduction bug to divide more than once at a time, although it can take unknown amounts of time for it to happen.
