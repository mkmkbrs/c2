import pygame
import random

# Classes

## Input
from cls.mouse import Mouse

## Game objects
from cls.game import Game
from cls.renderer import Renderer
from cls.gmap import Gmap
from cls.camera import Camera
from cls.entitymanager import EntityManager
from cls.eventhandler import EventHandler

## Entities
from cls.cell import Cell
from cls.prey import Prey
from cls.predator import Predator
from cls.tree import Tree, Fruit
from cls.zombie import Zombie

## Utilities
from cls.color import Color

def main():
    # Initialize PyGame
    game          = Game("Cells 2", "0.2.0")

    # Initialize game objects
    gmap          = Gmap(game, 2048, 2048) # (render.winw + 1000, render.winh + 1000)
    cam           = Camera(scrollspeed=10)
    mouse         = Mouse(cam.scroll, gmap.cellsize)
    color         = Color()

    entitymanager = EntityManager(game, gmap, 500)
    renderer      = Renderer(1920, 1080, gmap, cam, mouse, game.entities, game.clock, False, game.font)

    eventhandler  = EventHandler(renderer, entitymanager, gmap, cam, mouse, game.run, game.entities, color)

    # Initialize entities
    # FIXME: Big and ugly
    # FIXME: Move prey, predator and tree initialization to ENTTMGR func
    for i in range(10):
        entitymanager.create_entity(Prey(renderer.win, (gmap.mapw, gmap.maph), cam.scroll,
                                         (random.randrange(0, gmap.mapw-entitymanager.entt_size),
                                          random.randrange(0, gmap.maph-entitymanager.entt_size)),
                                         (entitymanager.entt_size, entitymanager.entt_size),random.choice(entitymanager.entt_colors),
                                         (0, 0), 0.7, (-0.001, 0.01)), "random_move")

    for i in range(10):
        entitymanager.create_entity(Predator(renderer.win, (gmap.mapw, gmap.maph), cam.scroll,
                                             (random.randrange(0, gmap.mapw-entitymanager.entt_size),
                                              random.randrange(0, gmap.maph-entitymanager.entt_size)),
                                             (entitymanager.entt_size, entitymanager.entt_size), random.choice(entitymanager.entt_colors),
                                             (0, 0), 1, (-0.001, 0.01)), "random_move")

    # Place first trees
    for i in range(3, 6):
        entitymanager.create_entity(Tree(renderer.win, (random.randrange(0, gmap.mapw), random.randrange(0, gmap.maph)),
                                         4, color.colors["green"], game.entities), "idle")

    # Main Loop
    while game.run:
        # Cap the framerate, deltatime
        dt = game.clock.tick(renderer.fps)

        # Update mouse position
        mouse.update()

        # Handle events (keyboard, mouse)
        eventhandler.update()

        # Renderer functions
        renderer.update()

        # Update grid color values, hash table, entity neighbors
        gmap.update()

        entitymanager.update()

if __name__ == "__main__":
    main()
