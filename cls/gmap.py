import pygame

class Gmap():
    "A game map"
    def __init__(self, game, mapw, maph):
        self.game = game

        self.mapw = mapw
        self.maph = maph

        self.grid = []
        self.cellsize = 64

        self.hash_table = {}
        self.hash_table_cells_count = int(((self.mapw - 0)/self.cellsize) ** 2)

        # Initialize the grid
        for row in range(self.mapw):
            # Create a list for each row
            self.grid.append([])

            for column in range(self.maph):
                # For each column add zero to the current row
                self.grid[row].append(0)

        # Initialize the hash table (by filling in with empty arrays)
        [self.hash_table.setdefault(i, []) for i in range(self.hash_table_cells_count)]

    def update_grid_cells_colors(self):
        for x in range(int(self.mapw/self.cellsize)):
            for y in range(int(self.maph/self.cellsize)):
                self.grid[x][y] = 0

        for e in self.game.entities:
            # Mark inhabited grid cell for visualisation
            if e.color is self.game.color.colors["red"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 1

            if e.color is self.game.color.colors["green"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 2

            if e.color is self.game.color.colors["blue"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 3

            if e.color is self.game.color.colors["yellow"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 4

            if e.color is self.game.color.colors["purple"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 5

            if e.color is self.game.color.colors["cyan"]:
                self.grid[int(e.pos[0])//self.cellsize][int(e.pos[1])//self.cellsize] = 6


    def update(self):
        self.update_grid_cells_colors()
