from cls.cell import Cell

class Zombie(Cell):
    def __init__(self, window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration):
        super().__init__(window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration)

        self.max_energy = 101
