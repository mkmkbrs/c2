import pygame


class Camera():
    "Scrolls the whole render surface"
    # BUG: Any number above 10 will cause camera to get OOB
    def __init__(self, scrollspeed=10):
        self.scroll = pygame.math.Vector2(0, 0)
        self.speed = scrollspeed
