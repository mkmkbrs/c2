import pygame
import random

from cls.entity import Entity
from cls.color import Color

color = Color()

class Fruit(Entity):
    def __init__(self, win, pos, size, color, parent):
        super().__init__(win, pos, size, color)

        self.energy = random.randrange(5, 40)
        self.parent = parent
        self.food_type = "plant"
        self.ctype = "fruit"

    def loose_energy(self):
        self.energy -= 0.01

    def update(self):
        self.loose_energy()
        super().update()


class Tree(Entity):
    def __init__(self, win, pos, size, color, entities):
        super().__init__(win, pos, size, color)

        self.entities = entities
        self.children = []

        self.energy = 1

        self.food_type = "none"
        self.ctype = "tree"

        for i in range(random.randrange(4, 12)):
            rand_pos = [random.randrange(self.pos[0]-100, self.pos[0]+100),
                        random.randrange(self.pos[1]-100, self.pos[1]+100)]

            # Make sure fruits don't spawn out of bounds
            # Check x
            if((rand_pos[0] > 2048 - self.size[0]) or (rand_pos[0] <= 1)): break
            # Check y
            if((rand_pos[1] > 2048 - self.size[1]) or (rand_pos[1] <= 1)): break

            self.fruit = Fruit(self.win, rand_pos, self.size, self.color, self)

            self.entities.append(self.fruit)
            self.children.append(self.fruit)


    def update(self):
        super().update()
