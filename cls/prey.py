from cls.cell import Cell

class Prey(Cell):
    def __init__(self, window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration):
        super().__init__(window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration)

        self.food_type = "meat"
        self.diet = ["plant"]

        self.ctype = "prey"
