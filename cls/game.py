import pygame
import random

from cls.color import Color

class Game:
    "Initializes the PyGame window and basic game components"
    def __init__(self, title, ver):
        self.title = title
        self.ver = ver

        self.run = True
        self.clock = pygame.time.Clock()

        self.fontpath = 'font/robotomono/RobotoMono-Regular.ttf'
        self.fontsize = 14

        self.color = Color()

        # Initialize PyGame
        pygame.init()
        pygame.display.set_caption(f'{self.title} v.{self.ver}')

        # Font should be initialized after window creation
        self.font = pygame.font.Font(self.fontpath, self.fontsize)

        # Set random seed
        random.seed()

        # Create entities list
        self.entities = []
