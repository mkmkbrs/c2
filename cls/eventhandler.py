import pygame
import random
from sys import exit as sys_exit

from cls.tree import Tree, Fruit


class EventHandler():
    def __init__(self, render, entitymanager, gmap, cam, mouse, run, entities, color):
        self.render = render
        self.entitymanager = entitymanager
        self.gmap = gmap
        self.cam  = cam
        self.mouse = mouse
        self.entities = entities
        self.color = color
        self.run = run

        # Timers (ms)
        self.t_grow_trees = random.randrange(7000, 10000)

        # Game Events
        self.ev_grow_trees = pygame.USEREVENT + 1

        # Initialize timers
        pygame.time.set_timer(self.ev_grow_trees, self.t_grow_trees)


    # TODO: Provide EventHandler with a control/input scheme file rather than
    #       hardcoded values
    def handle_keys(self, event):
        # Enable/Disable Debug UI
        if event.key == pygame.K_BACKQUOTE:
            if self.render.show_debug_ui is True:
                self.render.show_debug_ui = False
            else:
                self.render.show_debug_ui = True

        # Set/Unset Fullscreen Mode
        if event.key == (pygame.K_LALT and pygame.K_RETURN):
            self.render.set_fullscreen()

        # Exit
        if event.key == pygame.K_q:
            pygame.quit()
            sys_exit()


    def handle_camera_keys(self, keys_pressed, cam, render, gmap):
        # Up
        if keys_pressed[pygame.key.key_code("w")] or keys_pressed[pygame.key.key_code("k")] or keys_pressed[pygame.K_UP]:
            if cam.scroll[1] != 0:
                cam.scroll[1] += cam.speed

        # Left
        if keys_pressed[pygame.key.key_code("a")] or keys_pressed[pygame.key.key_code("h")] or keys_pressed[pygame.K_LEFT]:
            if cam.scroll[0] != 0:
                cam.scroll[0] += cam.speed

        # Down
        if keys_pressed[pygame.key.key_code("s")] or keys_pressed[pygame.key.key_code("j")] or keys_pressed[pygame.K_DOWN]:
            if cam.scroll[1] > (render.winh - gmap.maph):
                cam.scroll[1] -= cam.speed

        # Right
        if keys_pressed[pygame.key.key_code("d")] or keys_pressed[pygame.key.key_code("l")] or keys_pressed[pygame.K_RIGHT]:
            if cam.scroll[0] > (render.winw - gmap.mapw):
                cam.scroll[0] -= cam.speed


    def handle_camera_mouse(self, mouse, cam, render, gmap, border=70):
        # Up
        if mouse.pos.y <= border:
            if cam.scroll[1] != 0:
                cam.scroll[1] += cam.speed

        # Left
        if mouse.pos.x <= border:
            if cam.scroll[0] != 0:
                cam.scroll[0] += cam.speed

        # Down
        if mouse.pos.y >= render.winh - border:
            if cam.scroll[1] > (render.winh - gmap.maph):
                cam.scroll[1] -= cam.speed

        # Right
        if mouse.pos.x >= render.winw - border:
            if cam.scroll[0] > (render.winw - gmap.mapw):
                cam.scroll[0] -= cam.speed


    def update(self):
        keys_pressed = pygame.key.get_pressed()

        # Handle Camera
        self.handle_camera_keys(keys_pressed, self.cam, self.render, self.gmap)
        self.handle_camera_mouse(self.mouse, self.cam, self.render, self.gmap)

        for event in pygame.event.get():

            # Handle Game events
            # Plant trees
            if event.type == self.ev_grow_trees:
                for i in range(3, 6):
                    self.entitymanager.create_entity(Tree(self.render.win,
                                                     (random.randrange(0, self.gmap.mapw), random.randrange(0, self.gmap.maph)),
                                                     4, self.color.colors["green"], self.entities), self.entities)

            # Handle keys
            if event.type == pygame.KEYDOWN:
                self.handle_keys(event)

            # Quit
            if event.type == pygame.QUIT:
                self.run = False
                pygame.quit()
