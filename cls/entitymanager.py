import pygame
import random

from cls.color import Color
from cls.prey import Prey
from cls.predator import Predator

from func.fn_grid_tools import get_pos_to_grid, get_adj_grid_cells, get_adj_grid_ids

# TODO:
# - [ ] Handle states

class EntityManager:
    def __init__(self, game, gmap, entt_limit):
        self.game = game
        self.gmap = gmap
        self.entt_size = 4
        self.entt_limit = entt_limit
        self.color = Color()
        self.entt_colors = list(self.color.colors.values())[:5] # Use only first 5 colors


    def create_entity(self, entity, state="idle"):
        if len(self.game.entities) < self.entt_limit:
            # NOTE: Entities do not have 'states', but cells do
            try:
                entity.set_state(state)
            except AttributeError:
                pass

            self.game.entities.append(entity)
        # else: visual notification


    # FIXME: Lessen the nesting
    def handle_replication(self):
        for e in self.game.entities:
            if len(self.game.entities) < self.entt_limit:
                if round(e.energy) == 100:
                    if e.ctype == "predator":
                        new_e = Predator(e.win, (e.map_bounds[0], e.map_bounds[1]), e.scroll, e.pos,
                                         (self.entt_size, self.entt_size), e.color, (0, 0), 1, (-0.001, 0.01))
                    elif e.ctype == "prey":
                        # NOTE: Prey has a chance to spawn with random color
                        new_e = Prey(e.win, (e.map_bounds[0], e.map_bounds[1]), e.scroll, e.pos,
                                     (self.entt_size, self.entt_size), random.choice(self.entt_colors), (0, 0), 1, (-0.001, 0.01))

                    self.create_entity(new_e, "random_move")

                    e.energy = 50
                    new_e.energy = 50


    def handle_food_consumption(self):
        # React to food
        # FIXME: This is one ugly if statement
        for e in self.game.entities:
            if e.ctype == "prey" or e.ctype == "predator":
                if e.signal == 2:
                    # Add energy from food
                    if e.energy < e.max_energy:
                        if (e.energy + e._target.energy) > e.max_energy:
                            e.energy = e.max_energy
                        else:
                            e.energy += e._target.energy

                    # Remove the food item from entities list
                    # and from tree children list (if they have it)
                    if e._target in self.game.entities:
                        if hasattr(e._target, "parent"):
                            e._target.parent.children.pop(e._target.parent.children.index(e._target))
                            self.game.entities.pop(self.game.entities.index(e._target))
                        else:
                            self.game.entities.pop(self.game.entities.index(e._target))

                    e.target = None


    def handle_death_and_decay(self):
        # Entitiy becomes a corpse if it's energy reaches 0
        # Then it decays untill final death
        for e in self.game.entities:
            if e.energy < 1:
                # Fruits are removed from their parents children list
                if e.ctype == "fruit":
                    e.parent.children.pop(e.parent.children.index(e))
                    self.game.entities.pop(self.game.entities.index(e))
                else:
                    self.game.entities.pop(self.game.entities.index(e))

            # Remove childless trees
            if e.ctype == "tree" and e.children == []:
                self.game.entities.pop(self.game.entities.index(e))


    def update_entity_neighbors(self, gmap):
        # Re-fill hash table with new values on every frame update
        for value in gmap.hash_table.values():
            value.clear()

        try:
            for e in self.game.entities:
                self.gmap.hash_table[e.grid_id].append(e)
        except KeyError:
            pass


    # BUG: Returns negative key (e.grid_id) when entity bounces off the map borders
    def get_neighbors(self):
        for e in self.game.entities:
            e.neighbors.clear()

            try:
                n = self.gmap.hash_table[e.grid_id]

                # Add self
                # n = [n, ...] so we have to extract each element
                # to append them to the list instead of appending lists
                for i in n:
                    if i not in e.neighbors:
                        e.neighbors.append(i)
            except KeyError:
                pass


    def get_adj_neighbors(self):
        for e in self.game.entities:
            e.neighbors.clear()

            try:
                n = self.gmap.hash_table[e.grid_id]

                # Add self
                for i in n:
                    if i not in e.neighbors:
                        e.neighbors.append(i)

                # Add adjacent neighbors in 3x3 radius
                for a_id in e.adj_grid_ids:
                    a = self.gmap.hash_table[a_id]

                    for j in a:
                        if j not in e.neighbors:
                            e.neighbors.append(j)
            except KeyError:
                pass


    def update_entities(self):
        for e in self.game.entities:

            if e.ctype == "prey":
                self.get_adj_neighbors()

            if e.ctype == "predator":
                if e.energy >= 50:
                    self.get_neighbors()
                else:
                    self.get_adj_neighbors()

            e.update()


    def update(self):
        self.handle_replication()
        self.handle_food_consumption()
        self.handle_death_and_decay()

        self.update_entity_neighbors(self.gmap)
        self.update_entities()
