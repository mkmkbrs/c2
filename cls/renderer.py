import pygame

from collections import Counter
from cls.color import Color

from func.fn_grid_tools import get_pos_to_grid


class Renderer():
    def __init__(self, winw, winh, gmap, cam, mouse, entities, clock, dofullscreen, gamefont, show_debug_ui=True):
        self.fps = 60

        self.winw = winw
        self.winh = winh
        self.win = pygame.display.set_mode((winw, winh))

        self.gmap = gmap
        self.cam = cam
        self.mouse = mouse
        self.entities = entities
        self.clock = clock

        self.dofullscreen = dofullscreen

        self.gamefont = gamefont
        self.show_debug_ui = show_debug_ui

        self.color = Color()


    def draw_window_bg(self, win, color):
        win.fill(color)


    def draw_map_bounds(self, win, color, gmap, cam):
        pygame.draw.rect(win, color, pygame.Rect(0+cam.scroll[0], 0+cam.scroll[1],
                                                                gmap.mapw, gmap.maph), 1)


    def draw_grid(self, win, color, gmap, cam):
        for x in range(0, gmap.mapw, gmap.cellsize):
            pygame.draw.line(win, color, (x+cam.scroll[0], 0), (x+cam.scroll[0], gmap.mapw), 1)
        for y in range(0, gmap.maph, gmap.cellsize):
            pygame.draw.line(win, color, (0, y+cam.scroll[1]), (gmap.mapw, y+cam.scroll[1]), 1)


    def draw_grid_color_cells(self, win, color, gmap, cam):
        for x in range(int(gmap.mapw/gmap.cellsize)):
            for y in range(int(gmap.maph/gmap.cellsize)):
                rect = pygame.Rect(x*gmap.cellsize+cam.scroll[0], y*gmap.cellsize+cam.scroll[1],
                                   gmap.cellsize, gmap.cellsize)

                grid_cell_color = None

                if gmap.grid[x][y] == 1:
                    grid_cell_color = color.colors["dkred"]
                if gmap.grid[x][y] == 2:
                    grid_cell_color = color.colors["darkestgreen"]
                if gmap.grid[x][y] == 3:
                    grid_cell_color = color.colors["dkblue"]
                if gmap.grid[x][y] == 4:
                    grid_cell_color = color.colors["dkyellow"]
                if gmap.grid[x][y] == 5:
                    grid_cell_color = color.colors["dkpurple"]
                if gmap.grid[x][y] == 6:
                    grid_cell_color = color.colors["dkcyan"]

                if grid_cell_color is not None:
                    pygame.draw.rect(win, grid_cell_color, rect, 0)


    def draw_entities(self, win, color, entities, cam):
        # Draw only inside window boundaries
        for e in entities:
            if (e.pos[0] < (-cam.scroll[0]) + win.get_width()):
                if (e.pos[1] < (-cam.scroll[1]) + win.get_height()):
                    # Draw Tree branches
                    # BUG: Branches get drawn above rects despite draw order
                    if e.ctype == "tree":
                        for child in e.children:
                            if child in entities:
                                pygame.draw.line(win, color.colors["dkgreen"],
                                                 e.pos+cam.scroll, child.pos+child.size/2+cam.scroll, width=1)

                    # Draw self
                    pygame.draw.rect(win, e.color,
                                     pygame.Rect(e.pos[0]+cam.scroll[0], e.pos[1]+cam.scroll[1],
                                                 e.size[0], e.size[1]))

    def draw_fps(self, win, color, gamefont, clock):
        # FPS counter
        if (int(clock.get_fps()) in range(0, 24)):
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["red"])
        elif (int(clock.get_fps()) in range(25, 30)):
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["yellow"])
        else:
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["green"])

        win.blit(fps_counter, (24, 18))


    # NOTE: Might not be as relible as print() in some cases
    def draw_debug_ui(self, win, color, gamefont, clock, mouse, entities, cam):
        # Per Entity Info
        # TODO: Clean up, Separate initialization (debug_ui_init) and blitting
        #       (blit, loops), Here - add slots, same as for the debug panel
        for e in entities:
            if hasattr(e, 'energy'):
                entities_info_slot_1 = gamefont.render(f'{(round(e.energy))}', True, color.colors["white"])
                # entities_info_slot_1 = gamefont.render(f'{(e.find_adj_grid_cells((32, 32), e.get_pos_to_grid(e.pos, self.gmap.cellsize)))}', True, color.colors["white"])
                # entities_info_slot_1 = gamefont.render(f'{e.neighbors}', True, color.colors["white"])

                # entities_info_slot_1 = gamefont.render(f'{get_pos_to_grid(e.pos, 64)}', True, color.colors["white"])

            if hasattr(e, 'ctype'):
                entities_info_slot_2 = gamefont.render(f'{(e.ctype)}', True, color.colors["gray"])

            if hasattr(e, '_state'):
                entities_info_slot_3 = gamefont.render(f'{(e._state)}', True, color.colors["gray"])

                info_rect_1 = entities_info_slot_1.get_rect()
                info_rect_1.center = pygame.Rect(e.pos[0], e.pos[1], e.size[0], e.size[1]).center

                info_rect_2 = entities_info_slot_2.get_rect()
                info_rect_2.center = pygame.Rect(e.pos[0], e.pos[1], e.size[0], e.size[1]).center

                info_rect_3 = entities_info_slot_3.get_rect()
                info_rect_3.center = pygame.Rect(e.pos[0], e.pos[1], e.size[0], e.size[1]).center

                # Only draw text for entities inside the camera view
                if (e.pos[0] < (-cam.scroll[0]) + win.get_width()):
                    if (e.pos[1] < (-cam.scroll[1]) + win.get_height()):

                        win.blit(entities_info_slot_1, (info_rect_1.left + cam.scroll[0],
                                                        info_rect_1.bottom - e.size[1]*2 + cam.scroll[1]))

                        win.blit(entities_info_slot_2, (info_rect_2.left + cam.scroll[0],
                                                        info_rect_2.bottom - e.size[1] + 10 + cam.scroll[1]))

                        win.blit(entities_info_slot_3, (info_rect_3.left + cam.scroll[0],
                                                        info_rect_3.bottom - e.size[1] + 24 + cam.scroll[1]))

        # FPS counter
        if (int(clock.get_fps()) in range(0, 24)):
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["red"])
        elif (int(clock.get_fps()) in range(25, 30)):
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["yellow"])
        else:
            fps_counter = gamefont.render(f'FPS: {round(clock.get_fps())}', True, color.colors["green"])

        # Mouse Info
        mouse_info_header = gamefont.render('Mouse Info',                     True, color.colors["white"])

        mouse_screen_pos  = gamefont.render(f'Screen   {mouse.pos}',          True, color.colors["gray"])
        mouse_world_pos   = gamefont.render(f'World    {mouse.pos_to_world}', True, color.colors["gray"])
        mouse_grid_pos    = gamefont.render(f'Grid     {mouse.pos_to_grid}',  True, color.colors["gray"])
        mouse_grid_id     = gamefont.render(f'Grid ID  {mouse.grid_id}',      True, color.colors["gray"])

        # Camera Info (unused)
        # debugScroll = self.gamefont.render(f'Camera Scroll {camscroll}', True, self.color.colors["gray"])

        # Entities Info
        predator_pop_counter = (Counter([e.ctype == "predator" for e in entities])).get(True)
        prey_pop_counter     = (Counter([e.ctype == "prey"     for e in entities])).get(True)
        tree_pop_counter     = (Counter([e.ctype == "tree"     for e in entities])).get(True)
        fruit_pop_counter    = (Counter([e.ctype == "fruit"    for e in entities])).get(True)

        entities_info_header = gamefont.render('Entities Info', True, color.colors["white"])

        entities_overall     = gamefont.render(f'Overall  {len(entities)}',        True, color.colors["gray"])
        predator_pop         = gamefont.render(f'Predator {predator_pop_counter}', True, color.colors["gray"])
        prey_pop             = gamefont.render(f'Prey     {prey_pop_counter}',     True, color.colors["gray"])
        tree_pop             = gamefont.render(f'Trees    {tree_pop_counter}',     True, color.colors["gray"])
        fruit_pop            = gamefont.render(f'Fruit    {fruit_pop_counter}',    True, color.colors["gray"])

        # Color
        red_counter    = (Counter([e.color == color.colors["red"]    for e in entities])).get(True)
        blue_counter   = (Counter([e.color == color.colors["blue"]   for e in entities])).get(True)
        yellow_counter = (Counter([e.color == color.colors["yellow"] for e in entities])).get(True)
        purple_counter = (Counter([e.color == color.colors["purple"] for e in entities])).get(True)
        cyan_counter   = (Counter([e.color == color.colors["cyan"]   for e in entities])).get(True)
        dead_counter   = (Counter([e.color == color.colors["dkgray"] for e in entities])).get(True)

        entities_color_header = gamefont.render('Colors', True, color.colors["white"])

        red_cells    = gamefont.render(f'Red      {red_counter}',    True, color.colors["red"]    if (red_counter    != None) else color.colors["dkgray"])
        blue_cells   = gamefont.render(f'Blue     {blue_counter}',   True, color.colors["blue"]   if (blue_counter   != None) else color.colors["dkgray"])
        yellow_cells = gamefont.render(f'Yellow   {yellow_counter}', True, color.colors["yellow"] if (yellow_counter != None) else color.colors["dkgray"])
        purple_cells = gamefont.render(f'Purple   {purple_counter}', True, color.colors["purple"] if (purple_counter != None) else color.colors["dkgray"])
        cyan_cells   = gamefont.render(f'Cyan     {cyan_counter}',   True, color.colors["cyan"]   if (cyan_counter   != None) else color.colors["dkgray"])
        dead_cells   = gamefont.render(f'Dead     {dead_counter}',   True, color.colors["dkgray"])

        # Other
        separator = gamefont.render('', True, color.colors["black"])

        # Set up Slots
        padding_left   = 24
        padding_bottom = 18

        slots = [[padding_left, padding_bottom]]

        info_strings = [
            fps_counter,

            separator,

            mouse_info_header,
            mouse_screen_pos,
            mouse_world_pos,
            mouse_grid_pos,
            mouse_grid_id,

            separator,

            entities_info_header,
            entities_overall,
            predator_pop,
            prey_pop,
            tree_pop,
            fruit_pop,

            separator,

            red_cells,
            blue_cells,
            yellow_cells,
            purple_cells,
            cyan_cells,
            dead_cells
        ]

        slots_amount = len(info_strings)

        # Fill the slots with coordinates (x, prev_y + padding)
        for i in range(slots_amount):
            x = slots[i][0]
            y = slots[i][1] + padding_bottom
            slots.append([x, y])

        # Draw debug panel
        panel_w = 256
        panel_h = slots[-1][1] + padding_left

        debug_panel = pygame.Surface((panel_w, panel_h))
        debug_panel.set_alpha(190)
        debug_panel.fill(color.colors["black"])

        # Panel Border
        debug_panel_border_rect = pygame.Rect(0, 0, panel_w, panel_h)

        # Blitting
        win.blit(debug_panel, (0, 0))
        pygame.draw.rect(win, color.colors["dkgray"], debug_panel_border_rect, 1)

        for i in range(len(info_strings)):
            win.blit(info_strings[i], slots[i])


    def set_fullscreen(self):
        if not self.dofullscreen:
            pygame.display.set_mode((self.winw, self.winh), pygame.FULLSCREEN)
            self.dofullscreen = True
        else:
            pygame.display.set_mode((self.winw, self.winh))
            self.dofullscreen = False


    def update(self):
        # Window
        self.draw_window_bg(self.win, self.color.colors["black"])

        # Grid
        self.draw_grid_color_cells(self.win, self.color, self.gmap, self.cam)
        # self.draw_grid(self.win, self.color.colors["dkgray"], self.gmap, self.cam)
        # self.draw_map_bounds(self.win, self.color.colors["gray"], self.gmap, self.cam)

        # Entities
        self.draw_entities(self.win, self.color, self.entities, self.cam)

        # UI
        if self.show_debug_ui is True:
            self.draw_debug_ui(self.win, self.color, self.gamefont, self.clock, self.mouse, self.entities, self.cam)
        else:
            self.draw_fps(self.win, self.color, self.gamefont, self.clock)

        # Pygame
        pygame.display.update()
