import pygame
import math


class Mouse():
    def __init__(self, camscroll, gmapcellsize):
        self.self = self
        self.camscroll = camscroll
        self.gmapcellsize = gmapcellsize

        # TODO: Rename pos to screen_pos
        self.pos = pygame.math.Vector2(pygame.mouse.get_pos())
        self.pos_to_world = self.pos - self.camscroll
        self.pos_to_grid = pygame.math.Vector2(self.pos_to_world[0] // self.gmapcellsize,
                                               self.pos_to_world[1] // self.gmapcellsize)

        self.selstart = pygame.math.Vector2(0, 0)
        self.selend   = pygame.math.Vector2(0, 0)

        self.grid_id = 0;


    def get_grid_id(self):
        min_size_px = 0
        max_size_px = 2048
        cell_size = 64
        width = (max_size_px-min_size_px)/cell_size

        # Hash function
        grid_id = int((math.floor(self.pos_to_world[0]/cell_size)) + (math.floor(self.pos_to_world[1]/cell_size)) * width)

        return grid_id


    def make_selection(self, start, end, camscroll):
        selection = pygame.Rect(start[0] + camscroll[0], start[1] + camscroll[1],
                                end[0] - start[0], end[1] - start[1])
        return selection


    def update(self):
        self.pos = pygame.math.Vector2(pygame.mouse.get_pos())
        self.pos_to_world = self.pos - self.camscroll
        self.pos_to_grid = pygame.math.Vector2(self.pos_to_world[0] // self.gmapcellsize,
                                               self.pos_to_world[1] // self.gmapcellsize)
        self.grid_id = self.get_grid_id();
