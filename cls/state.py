class State(object):
    def __init__(self, name):
        self.name = name

    def update(self, dt_s):
        pass

    def check_conditions(self):
        pass

    def entry_actions(self):
        pass

    def exit_actions(self):
        pass
