import pygame
import math

from func.fn_grid_tools import get_grid_id, get_adj_grid_cells, get_pos_to_grid, get_adj_grid_ids


class Entity():
    def __init__(self, win, pos, size, color):
        self.self = self
        self.eid = None
        self.grid_id = 0
        self.win = win                          # A windown to be drawn on
        self.pos = pygame.math.Vector2(pos)     # (x, y)
        self.size = pygame.math.Vector2(size)
        self.color = color
        self.rect = pygame.Rect(self.pos[0], self.pos[1], self.size[0], self.size[1])

        self.neighbors = [None] * 18 # Limit
        self.adj_grid_ids = []


    def update(self):
        # Update rect
        self.rect = pygame.Rect(self.pos[0], self.pos[1], self.size[0], self.size[1])

        # Update Grid ID
        self.grid_id = get_grid_id(self.pos, 64, 2048, 0)

        # Update adjacent Grid IDs
        adj_grid_cells = get_adj_grid_cells((2048, 2048), get_pos_to_grid(self.pos, 64))
        self.adj_grid_ids = get_adj_grid_ids(adj_grid_cells, 64, 2048)
