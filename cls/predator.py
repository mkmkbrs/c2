from cls.cell import Cell


class Predator(Cell):
    def __init__(self, window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration):
        super().__init__(window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration)

        self.max_energy = 110

        self.food_type = "meat"
        self.diet = ["meat", "corpse"]

        self.ctype = "predator"


    def check_for_food(self):
        if self.neighbors is not None:

            for n in self.neighbors:
                # if neighbor.ctype is "prey":
                if n.color is not self.color:
                    if n.food_type == "meat":
                        self._target = n
                        self.set_state("move_to")
