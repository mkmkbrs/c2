class Color():
    def __init__(self):
        self.colors = {
            "red": (242, 38, 19),
            "blue": (30, 144, 255),
            "yellow": (226, 165, 14),
            "purple": (244, 0, 244),
            "cyan": (0, 164, 166),
            "green": (0, 212, 0),
            "black": (0, 0, 0),
            "white": (196, 196, 196),
            "gray": (128, 128, 128),
            "dkgray": (64, 64, 64),
            "dkred": (24, 10, 10),
            "dkgreen": (0, 104, 0),
            "dkblue": (10, 10, 24),
            "dkyellow": (24, 24, 10),
            "dkpurple": (24, 10, 24),
            "dkcyan": (10, 24, 24),
            "darkestgreen": (10, 24, 10)
        }
