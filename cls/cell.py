import pygame
import math
import numpy as np
import random

from cls.entity import Entity
from cls.color import Color

from cls.tree import Fruit

# from func.fn_get_target import get_target
from func.fn_grid_tools import get_adj_grid_cells, get_pos_to_grid, get_grid_id, get_adj_grid_ids

color = Color()

class Cell(Entity):
    def __init__(self, window, map_bounds, scroll, pos, size, color, velocity, max_velocity, acceleration):
        super().__init__(window, pos, size, color)

        self.energy = 99
        self.max_energy = 103

        self.signal = 0
        self.color = color

        self.map_bounds = pygame.math.Vector2(map_bounds)
        self.scroll = scroll

        self.velocity = pygame.math.Vector2(velocity)
        self.max_velocity = max_velocity

        self.acceleration = pygame.math.Vector2(acceleration)

        self._states = [
            "idle",
            "random_move",
            "move_to",
            "move_away",
            "decay"
        ]

        self._state = self._states[0]

        # TODO: Hungry cells (below 50) have extended (3x3) grid_id visibility
        # self._neighbors = [] # None
        self._target = None

        self.food_type = "meat"
        self.diet = ["plant"]

        self.ctype = "prey"


    def check_bounds(self, width, height):
        # Check x
        if((self.pos[0] > width - self.size[0]) or (self.pos[0] <= 1)):  #inb4 - = 0
            self.velocity[0] = self.velocity[0] * -1
        # Check y
        if((self.pos[1] > height - self.size[1]) or (self.pos[1] <= 1)): #inb4 - = 0
            self.velocity[1] = self.velocity[1] * -1


    def limit_velocity(self, limit):
        if abs(self.velocity[0]) >= limit:
            # Normalize x
            self.velocity[0] = self.velocity[0]/abs(self.velocity[0]) * limit
        if abs(self.velocity[1]) >= limit:
            # Normalize y
            self.velocity[1] = self.velocity[1]/abs(self.velocity[1]) * limit


    def check_for_food(self):
        # if self.neighbors is not None:
        for n in self.neighbors:
            if n is not self:
                if hasattr(n, "food_type"):
                    if n.food_type in self.diet and n.ctype is not self.ctype:
                        self._target = n
                        self.set_state("move_to")


    def check_for_predators(self):
        for n in self.neighbors:
            if n is not None and n.ctype == "predator" and self.ctype == "prey":
                self._target = n
                self.set_state("move_away")


    def idle(self):
        pass


    def random_move(self):
        self.signal = 0

        self.acceleration = (random.uniform(-0.05, 0.05), random.uniform(-0.05, 0.05))
        self.velocity += self.acceleration
        self.limit_velocity(self.max_velocity)
        self.pos += self.velocity
        self.check_bounds(self.map_bounds[0], self.map_bounds[1])

        # Don't search for food if saturated
        if self.energy <= 99:
            self.check_for_food()

        self.check_for_predators()


    def set_state(self, new_state):
        n = self._states.index(new_state)

        if self._state != n:
            self._state = self._states[n]


    def get_state(self):
        return self._state


    def move_to(self, target):
        if self._target in self.neighbors:
            self.set_state("move_to")
            target = pygame.math.Vector2(target)

            # Sub
            dx = target[0] - self.pos[0]
            dy = target[1] - self.pos[1]

            direction = pygame.math.Vector2(dx, dy)

            # Normalize
            direction[0] = direction[0] / np.sqrt(np.sum(direction[0]**2))
            direction[1] = direction[1] / np.sqrt(np.sum(direction[1]**2))

            # Mult
            # NOTE: Old value - 0.05, new - 0.10 seems to fix never arriving behavior
            direction = direction * 0.10

            self.acceleration = direction

            self.velocity += self.acceleration
            self.limit_velocity(self.max_velocity)
            self.pos += self.velocity

            # The mover stops if it is approximately 2% (?) close to the target
            rel_tol = 0.009

            self.check_bounds(self.map_bounds[0], self.map_bounds[1])

            if (math.isclose(self.pos[0]/self.size[0], target[0]/self.size[0], rel_tol=rel_tol) and math.isclose(self.pos[1]/self.size[1], target[1]/self.size[1], rel_tol=rel_tol)):
                self.velocity = pygame.math.Vector2(0, 0)

                if self._target.food_type in self.diet:
                    self.signal = 2

                self.set_state("random_move")
        else:
            self.set_state("random_move")


    def move_away(self, target):
        if self._target in self.neighbors:
            self.set_state("move_away")
            target = pygame.math.Vector2(target)

            # Sub
            dx = target[0] - self.pos[0]
            dy = target[1] - self.pos[1]

            direction = pygame.math.Vector2(-dx, -dy)

            # Normalize
            direction[0] = direction[0] / np.sqrt(np.sum(direction[0]**2))
            direction[1] = direction[1] / np.sqrt(np.sum(direction[1]**2))

            # Mult
            direction = direction * 0.05

            self.acceleration = direction

            self.velocity += self.acceleration
            self.limit_velocity(self.max_velocity)
            self.pos += self.velocity

            self.check_bounds(self.map_bounds[0], self.map_bounds[1])
        else:
            self.set_state("random_move")


    # NOTE: Wierd acceleration and deceleration, but less taxing on resources
    #       when large amounts of entities are moving at the same time.
    def old_move_to(self, target):
        self.set_state("move_to")
        target = pygame.math.Vector2(target)

        dx = target[0] - self.pos[0]
        dy = target[1] - self.pos[1]

        direction = pygame.math.Vector2(dx, dy)

        self.velocity += self.acceleration
        self.limit_velocity(self.max_velocity)

        self.pos[0] += direction[0] * 0.05
        self.pos[1] += direction[1] * 0.05

        # The mover stops if it is approximately 0.5% close to the target
        rel_tol = 0.005

        if (math.isclose(self.pos[0], target[0], rel_tol=rel_tol) and math.isclose(self.pos[1], target[1], rel_tol=rel_tol)):
            self.velocity = pygame.math.Vector2(0, 0)

            if self._target.food_type in self.diet:
                self.signal = 2

            self.set_state("random_move")


    def decay(self):
        pass


    def switch_state(self):
        if self._state == "idle":
            self.idle()

        if self._state == "random_move":
            self.random_move()

        if self._state == "move_to":
            self.move_to(self._target.pos)
            # mousepos = pygame.math.Vector2(pygame.mouse.get_pos())
            # mouse_pos_to_world = pygame.math.Vector2(mousepos[0] - self.scroll[0], mousepos[1] - self.scroll[1])

            # self.move_to(mouse_pos_to_world)

        if self._state == "move_away":
            self.move_away(self._target.pos)

        if self._state == "decay":
            self.decay()


    def loose_energy(self):
        self.energy -= 0.01


    def update(self):
        # Update state
        self.switch_state()

        self.loose_energy()

        # Decay
        # TODO: Move it to it's own function
        if self.energy < 2 and self._state != "decay":
            self.color = color.colors["dkgray"]
            self.energy = random.randrange(5, 20)
            self.ctype = "prey"
            self.food_type = "corpse"
            self.set_state("decay")


        # FIXME: Magic Numbers
        # adj_grid_cells = get_adj_grid_cells(self.map_bounds, get_pos_to_grid(self.pos, 64))
        # self.adj_grid_ids = get_adj_grid_ids(adj_grid_cells, 64, 2048)

        # BUG: Because this check is happening here the states are ignored.
        #      Meaning the corpse can be scared by the predator and walk away.
        #      This bug results in a zombie party in a 100% of cases.
        #
        #      - Zombies can be resurrected from the "decay" state as long as the
        #        condition is met.
        #
        #      - Zombies can replicate ON THE SPOT seemingly without any clear
        #        reason.
        #
        #      - They also somehow can be born BOTH prey and predator despite
        #        inheritance.
        #
        #      - They also can be stuck in a self-eating loop which produces
        #        about 8-10 new zombies, when the regular cells can produce only
        #        one offspring at a time.
        #
        # TODO: Fix it and re-implement as a feature.
        # if self._neighbors is not None:
        #     for neighbor in self._neighbors:
        #         if neighbor.ctype == "predator" and self.ctype == "prey":
        #             self._target = neighbor
        #             self.set_state("move_away")

        # Update target
        # try:
            # NOTE: It doesn't matter that the _target contains self. You can later
            #       check if neighbor is not self in other methods without butchering
            #       the hash_table[key] values.

            # NOTE: It should be possible to get neighboring cells around the self.grid_id
            #       if necessary.

            # Returns list of objects
            # If you wan't to get specific attributes, iterate upon it
            # self._neighbors = self.hash_table[self.grid_id]
            # print(self.hash_table)
        #     pass
        # except KeyError:
        #     pass

        # print(f'Cell: {self._neighbors}')
        super().update()

